package com.example.lab03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.lab03.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegistrar.setOnClickListener {
            if(binding.edtNombre.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresa nombre", Toast.LENGTH_SHORT).show()
                binding.edtNombre.requestFocus()
                return@setOnClickListener
            }
            if(binding.edtEdad.text.toString().isEmpty()){
                Toast.makeText(this,"Ingresa edad", Toast.LENGTH_SHORT).show()
                binding.edtEdad.requestFocus()
                return@setOnClickListener
            }
            val nombreMascota = binding.edtNombre.text.toString()
            val edadMascota = binding.edtEdad.text.toString().toInt()

            var tipoMascota = ""
            if(binding.rdbPerro.isChecked){
                tipoMascota = binding.rdbPerro.text.toString()
            }
            if(binding.rdbGato.isChecked){
                tipoMascota = binding.rdbGato.text.toString()
            }
            if(binding.rdbConejo.isChecked){
                tipoMascota = binding.rdbConejo.text.toString()
            }

            if(tipoMascota.toString().isEmpty()){
                Toast.makeText(this,"Selecciona entre Perro,Gato o Conejo.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var vacunasMascotas = ""
            if(binding.chkDistemper.isChecked){
                vacunasMascotas += binding.chkDistemper.text.toString() + " "
            }
            if(binding.chkParvovirus.isChecked){
                vacunasMascotas += binding.chkParvovirus.text.toString() + " "
            }
            if(binding.chkLeptospirosis.isChecked){
                vacunasMascotas += binding.chkLeptospirosis.text.toString() + " "
            }
            if(vacunasMascotas.toString().isEmpty()){
                Toast.makeText(this,"Selecciona una vacuna.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            vacunasMascotas = vacunasMascotas.trim()

            println(nombreMascota)
            println(edadMascota)
            println(tipoMascota)
            println(vacunasMascotas)

            /*Ahora Creamos el Intent para enviar los datos en la segunda pantalla*/
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombreMascota",nombreMascota)
                putInt("key_edadMascota",edadMascota)
                putString("key_tipoMascota",tipoMascota)
                putString("key_vacunasMascotas",vacunasMascotas)
            }

            val intent= Intent(this,DetalleActivity::class.java).apply {
                    putExtras(bundle)
            }
            startActivity(intent)

        }
    }
}