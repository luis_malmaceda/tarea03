package com.example.lab03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.lab03.databinding.ActivityDetalleBinding
import com.example.lab03.databinding.ActivityMainBinding

class DetalleActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetalleBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetalleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle : Bundle? = intent.extras
        bundle?.let {
            val nombre = it.getString("key_nombreMascota","Sin Origen")
            val edad = it.getInt("key_edadMascota",0)
            val vacunas = it.getString("key_vacunasMascotas","Sin Origen")
            val tipo = it.getString("key_tipoMascota","")

            binding.tvNombre.text = nombre
            binding.tvEdad.text = edad.toString()
            binding.tvVacunas.text = vacunas
            when(tipo){
                "Perro"->{
                    binding.imgTipo.setImageResource(R.drawable.perro)
                }
                "Gato"->{
                    binding.imgTipo.setImageResource(R.drawable.gato)
                }
                "Conejo"->{
                    binding.imgTipo.setImageResource(R.drawable.conejo)
                }else ->{
                    binding.imgTipo.setImageResource(R.drawable.ic_launcher_background)
                }
            }


        }
    }
}